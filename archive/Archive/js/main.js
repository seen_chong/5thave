// Property of sean cooper

jQuery(function($)  
{
    $("#contact-form").submit(function()
    {
        var email = $("#email").val(); // get email field value
        var fname = $("#fname").val(); // get first name field value
        var lname = $("#lname").val(); // get last name field value
        var msg = $("#msg").val(); // get message field value
        $.ajax(
        {
            type: "POST",
            url: "https://mandrillapp.com/api/1.0/messages/send.json",
            data: {
                // 5th Ave Mandrill Key
                'key': 'PlPUbK-hJLYpPIeZiMeuZg',
                'message': {
                    'from_email': email,
                    'from_name': name,
                    'headers': {
                        'Reply-To': email
                    },
                    'subject': '5th Ave Email Submission',
                    'text': msg,
                    'to': [
                    {
                        'email': 'sean@emagid.com',
                        'name': 'Sean',
                        'type': 'to'
                    }]
                }
            }
        })
        .done(function(response) {
            alert('Thank You For Signing Up!'); // show success message
            $("#fname").val(''); // reset field after successful submission
            $("#lname").val(''); // reset field after successful submission
            $("#email").val(''); // reset field after successful submission
            $("#msg").val(''); // reset field after successful submission
        })
        .fail(function(response) {
            alert('Error sending message.');
        });
        return false; // prevent page refresh
    });
});